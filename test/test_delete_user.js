import chai from 'chai';
import chaiHttp from 'chai-http';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";
import models from "../src/models";
import http from "../config/http.json";

chai.use(chaiHttp);
chai.should();

describe(`DELETE /api/v${rest.version}/users/:id`, () => {

  beforeEach((done) => {
    let init = async () => {
      await TestHTTPService.start();

      await models.sequelize.drop();
      await models.sequelize.sync();

      let user1 = await models.user.create({
        username: 'user1',
        password: 'password1'
      });
      let user2 = await models.user.create({
        username: 'user2',
        password: 'password2'
      });

      let task1 = await models.task.create({
        title: 'Task 1',
        userId: user1.id
      });
      await task1.addFollower(user2);

      done();
    }

    init();
  });

  afterEach((done) =>{
    models.sequelize.drop().then(() => {
      done();
    });
  });

  it('Delete user without token', (done) => {
    chai.request(TestHTTPService.connection)
      .delete(`/api/v${rest.version}/users/1`)
      .end((error, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('Delete user', (done) => {
    chai.request(TestHTTPService.connection)
      .delete(`/api/v${rest.version}/users/1`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .end((error, res) => {
        res.should.have.status(200);

        models.user.findOne({
          where: {
            id: 1
          }
        }).then(async (user) => {
          (user == null).should.eql(true);
          
          models.task.findOne({
            where: {
              id: 1,
              userId: 1
            }
          }).then(task => {
            (task == null).should.eql(true);
            done();
          });
        })
      });
  });

  it('Delete user if not is owner', (done) => {
    chai.request(TestHTTPService.connection)
      .delete(`/api/v${rest.version}/users/2`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .end((error, res) => {
        res.should.have.status(403);
        res.should.to.be.json;
        res.body.should.be.eql({
          errors: {
            user: [`Can't delete (only owner)`]
          }
        });

        models.user.findOne({
          where: {
            id: 2
          }
        }).then(async (user) => {
          user.id.should.be.eql(2);
          user.username.should.be.eql('user2');
          done();
        })
      });
  });
});
