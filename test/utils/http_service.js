import HTTPService from '../../src/http/service';

var connection;

class TestHTTPService {
  async start (done, port = 25081) {
    if (!connection) {
      connection = await HTTPService.start(port);
    }

    if (done) {
      done();
    }
  }

  async stop(done) {
    connection.close();
    if (done) {
      done();
    }
  }

  get connection () {
    return connection;
  }
}

export default new TestHTTPService();