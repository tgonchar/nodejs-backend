import chai from 'chai';
import chaiHttp from 'chai-http';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";

chai.use(chaiHttp);
chai.should();

describe('GET /api', () => {
  beforeEach((done) => {
    TestHTTPService.start(done);
  });

  afterEach((done) =>{
    done();
  });

  it('Get alive and some information about api', (done) => {
    chai.request(TestHTTPService.connection)
      .get('/api')
      .end((error, res) => {
        res.should.have.status(200);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: {
            version: rest.version
          }
        });
        done();
      });
  });
});
