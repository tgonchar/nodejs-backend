import chai from 'chai';
import chaiHttp from 'chai-http';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";
import models from "../src/models";
import http from "../config/http.json";

chai.use(chaiHttp);
chai.should();

describe(`GET /api/v${rest.version}/tasks`, () => {

  beforeEach((done) => {
    let init = async () => {
      await TestHTTPService.start();

      await models.sequelize.drop();
      await models.sequelize.sync({ force: true });      

      let user1 = await models.user.build({
        username: 'user1',
        password: 'password1'
      }).save();
      let user2 = await models.user.build({
        username: 'user2',
        password: 'password2'
      }).save();
      let user3 = await models.user.build({
        username: 'user3',
        password: 'password3'
      }).save();

      let task1 = await models.task.create({
        title: 'Test task 1',
        userId: user1.id
      });
      await task1.addFollower(user2);
      await task1.addFollower(user3);

      let task2 = await models.task.create({
        title: 'Test task 2',
        userId: user1.id
      });
      await task2.addFollower(user1);
      await task2.addFollower(user3);

      let task3 = await models.task.create({
        title: 'Test task 3',
        userId: user2.id
      });
      await task3.addFollower(user1);

      done();
    }

    init();
  });

  afterEach((done) =>{
    models.sequelize.drop().then(() => {
      models.task.drop().then(() => {
        done();
      });
    });
  });

  it('Get users list without token', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/tasks`)
      .end((error, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('Get task list without offset and limit', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/tasks/share`)
      .set({
        token: jwt.sign({
          id: 2,
          username: 'user2'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .end((error, res) => {
        res.should.have.status(200);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: [
            {
              id: 1,
              title: 'Test task 1',
              userId: 1,
              followers: [
                {
                  id: 2,
                  username: 'user2'
                },
                {
                  id: 3,
                  username: 'user3'
                }
              ]
            }
          ]
        });
        done();
      });
  });

  it('Get tasks list with offset and limit', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/tasks/share`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .send({
        offset: 0,
        limit: 1
      })
      .end((error, res) => {
        res.should.have.status(200);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: [
            {
              id: 2,
              title: 'Test task 2',
              userId: 1,
              followers: [
                {
                  id: 1,
                  username: 'user1'
                },
                {
                  id: 3,
                  username: 'user3'
                }
              ]
            }
          ]
        });
        
        chai.request(TestHTTPService.connection)
          .get(`/api/v${rest.version}/tasks/share`)
          .set({
            token: jwt.sign({
              id: 1,
              username: 'user1'
            }, http.token.secret, {
              expiresIn: http.token.expires
            })
          })
          .send({
            offset: 1,
            limit: 1
          })
          .end((error, res) => {
            res.should.have.status(200);
            res.should.to.be.json;
            res.body.should.be.eql({
              data: [
                {
                  id: 3,
                  title: 'Test task 3',
                  userId: 2,
                  followers: [
                    {
                      id: 1,
                      username: 'user1'
                    }
                  ]
                }
              ]
            });
            done();
          });
      });
  });

  // it('Get tasks list with large offset', (done) => {
  //   chai.request(TestHTTPService.connection)
  //     .get(`/api/v${rest.version}/tasks`)
  //     .set({
  //       token: jwt.sign({
  //         id: 1,
  //         username: 'user1'
  //       }, http.token.secret, {
  //         expiresIn: http.token.expires
  //       })
  //     })
  //     .send({
  //       offset: 100,
  //       limit: 3
  //     })
  //     .end((error, res) => {
  //       res.should.have.status(200);
  //       res.should.to.be.json;
  //       res.body.should.be.eql([]);
  //       done();
  //     });
  // });
});
