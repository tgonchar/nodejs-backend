import chai from 'chai';
import chaiHttp from 'chai-http';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";
import models from "../src/models";
import http from "../config/http.json";

chai.use(chaiHttp);
chai.should();

describe(`POST /api/v${rest.version}/users/sign_in`, () => {
  beforeEach((done) => {
    let init = async () => {
      await TestHTTPService.start();

      await models.sequelize.drop();
      await models.sequelize.sync();

      let newUser = models.user.build({
        username: 'test',
        password: 'secret'
      });
      await newUser.save();
      done();
    }

    init();
  });

  afterEach((done) =>{
    models.sequelize.drop().then(() => {
      done();
    });
  });

  it('Simple signup', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/users/sign_in`)
      .send({
        username: 'test',
        password: 'secret'
      })
      .end((error, res) => {
        res.should.have.status(200);
        res.should.to.be.json;
        res.body.should.be.eql({
          token: jwt.sign({
            id: 1,
            username: 'test'
          }, http.token.secret, {
            expiresIn: http.token.expires
          })
        });

        done();
      });
  });

  it('Another password', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/users/sign_in`)
      .send({
        username: 'test',
        password: 'somesecret'
      })
      .end((error, res) => {
        res.should.have.status(401);
        res.should.to.be.json;
        res.body.should.be.eql({
          errors: {
            user: ['Authenticate is failed']
          }
        });
        done();
      });
  });

  it('Username and password length less 3', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/users/sign_in`)
      .send({
        username: '11',
        password: '11'
      })
      .end((error, res) => {
        res.should.have.status(415);
        res.should.to.be.json;
        res.body.should.be.eql({
          errors: {
            password: ['Password length must be 3-20'],
            username: ['Username length must be 3-20']
          }
        });
        done();
      });
  });

  it('Username and password length more 20', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/users/sign_in`)
      .send({
        username: '123456789012345678901',
        password: '123456789012345678901'
      })
      .end((error, res) => {
        res.should.have.status(415);
        res.should.to.be.json;
        res.body.should.be.eql({
          errors: {
            password: ['Password length must be 3-20'],
            username: ['Username length must be 3-20']
          }
        });
        done();
      });
  });

  it('Username not valid format', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/users/sign_in`)
      .send({
        username: '*@qq',
        password: 'test'
      })
      .end((error, res) => {
        res.should.have.status(415);
        res.should.to.be.json;
        res.body.should.be.eql({
          errors: {
            username: [`Username Can't only a-z, A-Z, 0-9`]
          }
        });
        done();
      });
  });
});
