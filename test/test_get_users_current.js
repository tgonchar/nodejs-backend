import chai from 'chai';
import chaiHttp from 'chai-http';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";
import http from "../config/http.json";
import models from "../src/models";

chai.use(chaiHttp);
chai.should();

describe(`GET /api/v${rest.version}/users/current`, () => {

  beforeEach((done) => {
    let init = async () => {
      await TestHTTPService.start();

      await models.sequelize.drop();
      await models.sequelize.sync();

      let newUser = models.user.build({
        username: 'test',
        password: 'secret'
      });
      await newUser.save();
      done();
    }

    init();
  });

  afterEach((done) => {
    models.sequelize.drop().then(() => {
      done();
    });
  });

  it('Get current user', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/users/current`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'test'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .end((error, res) => {
        res.should.have.status(200);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: {
            id: 1,
            username: 'test'
          }
        });
        done();
      });
  });

  it('Get current user without token', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/users/current`)
      .end((error, res) => {
        res.should.have.status(401);
        done();
      });
  });
});
