import chai from 'chai';
import chaiHttp from 'chai-http';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";
import models from "../src/models";
import http from "../config/http.json";

chai.use(chaiHttp);
chai.should();

describe(`PATCH /api/v${rest.version}/tasks/:id`, () => {

  beforeEach((done) => {
    let init = async () => {
      await TestHTTPService.start();

      await models.sequelize.drop();
      await models.sequelize.sync({ force: true });      

      let user1 = await models.user.build({
        username: 'user1',
        password: 'password1'
      }).save();
      let user2 = await models.user.build({
        username: 'user2',
        password: 'password2'
      }).save();
      let user3 = await models.user.build({
        username: 'user3',
        password: 'password3'
      }).save();

      let task1 = await models.task.create({
        title: 'Test task 1',
        userId: user1.id
      });
      await task1.addFollower(user2);
      await task1.addFollower(user3);

      done();
    }

    init();
  });

  afterEach((done) =>{
    models.sequelize.drop().then(() => {
      models.task.drop().then(() => {
        done();
      });
    });
  });

  it('Patch task without token', (done) => {
    chai.request(TestHTTPService.connection)
      .patch(`/api/v${rest.version}/tasks/1`)
      .end((error, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('Patch task', (done) => {
    chai.request(TestHTTPService.connection)
      .patch(`/api/v${rest.version}/tasks/1`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .send({
        title: '!!!',
        userId: 100,
        id: 100,
        followers: [{
          id: 2,
          username: 'user2'
        }]
      })
      .end(async (error, res) => {
        res.should.have.status(200);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: {
            id: 1,
            title: '!!!',
            userId: 1,
            followers: [
              {
                id: 2,
                username: 'user2'
              }
            ]
          }
        });

        let task = await models.task.findById(1);
        task.title.should.be.eql('!!!');
        task.userId.should.be.eql(1);

        let followers = await task.getFollower();
        followers.length.should.be.eql(1);
        followers[0].id.should.be.eql(2);
        followers[0].username.should.be.eql('user2');

        done();
      });
  });
});
