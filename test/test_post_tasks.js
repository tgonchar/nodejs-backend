import chai from 'chai';
import chaiHttp from 'chai-http';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";
import models from "../src/models";
import http from "../config/http.json";

chai.use(chaiHttp);
chai.should();

describe(`POST /api/v${rest.version}/tasks`, () => {

  beforeEach((done) => {
    let init = async () => {
      await TestHTTPService.start();

      await models.sequelize.drop();
      await models.sequelize.sync();

      let user1 = await models.user.create({
        username: 'user1',
        password: 'password1'
      });
      let user2 = await models.user.create({
        username: 'user2',
        password: 'password2'
      });
      let user3 = await models.user.create({
        username: 'user3',
        password: 'password3'
      });

      done();
    }

    init();
  });

  afterEach((done) =>{
    models.sequelize.drop().then(() => {
      done();
    });
  });

  it('Get users list without token', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/tasks`)
      .end((error, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('Simple add task', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/tasks`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .send({
        title: 'Title 1',
        followers: [
          {
            id: 2,
            username: 'user2'
          },
          {
            id: 3,
            username: 'user3'
          }
        ]
      })
      .end((error, res) => {
        res.should.have.status(201);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: {
            id: 1,
            title: 'Title 1',
            followers: [
              {
                id: 2,
                username: 'user2'
              },
              {
                id: 3,
                username: 'user3'
              }
            ]
          }
        });

        models.task.findOne({
          where: {
            id: 1
          }
        }).then(async (task) => {
          task.id.should.be.eql(1);
          task.title.should.be.eql('Title 1');
          let followers = await task.getFollower();
          followers.length.should.be.eql(2);
          followers[0].id.should.be.eql(2);
          followers[0].username.should.be.eql('user2');
          followers[1].id.should.be.eql(3);
          followers[1].username.should.be.eql('user3');
          done();
        })
      });
  });

  it('Without title', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/tasks`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .send({
        followers: []
      })
      .end((error, res) => {
        res.should.have.status(415);
        res.should.to.be.json;
        res.body.should.be.eql({
          errors: {
            title: ["Title can't be blank"]
          }
        });
        done();
      });
  });

  it('When follower is not exist', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/tasks`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .send({
        title: 'Title 1',
        followers: [
          {
            id: 100,
            username: 'user100'
          }
        ]
      })
      .end((error, res) => {
        res.should.have.status(201);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: {
            id: 1,
            title: 'Title 1',
            followers: []
          }
        });

        models.task.findOne({
          where: {
            id: 1
          }
        }).then(async (task) => {
          task.id.should.be.eql(1);
          task.title.should.be.eql('Title 1');
          let followers = await task.getFollower();
          followers.length.should.be.eql(0);
          done();
        })
      });
  });
});
