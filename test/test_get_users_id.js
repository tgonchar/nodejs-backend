import chai from 'chai';
import chaiHttp from 'chai-http';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";
import models from "../src/models";
import http from "../config/http.json";

chai.use(chaiHttp);
chai.should();

describe(`GET /api/v${rest.version}/users/:id`, () => {

  beforeEach((done) => {
    let init = async () => {
      await TestHTTPService.start();

      await models.sequelize.drop();
      await models.sequelize.sync({ force: true });

      let user1 = await models.user.build({
        username: 'user1',
        password: 'password1'
      }).save();

      done();
    }

    init();
  });

  afterEach((done) =>{
    models.sequelize.drop().then(() => {
      models.task.drop().then(() => {
        done();
      });
    });
  });

  it('Get user without token', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/users/1`)
      .end((error, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('Get user', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/users/1`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .end(async (error, res) => {
        res.should.have.status(200);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: {
            id: 1,
            username: 'user1'
          }
        });
        done();
      });
  });

  it('Get empty user', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/users/100`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .end(async (error, res) => {
        res.should.have.status(404);
        res.should.to.be.json;
        res.body.should.be.eql({
          errors: {
            user: [`Can't find`]
          }
        });
        done();
      });
  });
});
