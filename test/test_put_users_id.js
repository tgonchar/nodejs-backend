import chai from 'chai';
import chaiHttp from 'chai-http';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";
import models from "../src/models";
import http from "../config/http.json";

chai.use(chaiHttp);
chai.should();

describe(`PUT /api/v${rest.version}/users/:id`, () => {

  beforeEach((done) => {
    let init = async () => {
      await TestHTTPService.start();

      await models.sequelize.drop();
      await models.sequelize.sync({ force: true });      

      let user1 = await models.user.build({
        username: 'user1',
        password: 'password1'
      }).save();
      let user2 = await models.user.build({
        username: 'user2',
        password: 'password2'
      }).save();
      let user3 = await models.user.build({
        username: 'user3',
        password: 'password3'
      }).save();

      let task1 = await models.task.create({
        title: 'Test task 1',
        userId: user1.id
      });
      await task1.addFollower(user2);
      await task1.addFollower(user3);

      done();
    }

    init();
  });

  afterEach((done) =>{
    models.sequelize.drop().then(() => {
      models.task.drop().then(() => {
        done();
      });
    });
  });

  it('Put user without token', (done) => {
    chai.request(TestHTTPService.connection)
      .put(`/api/v${rest.version}/users/1`)
      .end((error, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('Put user update', (done) => {
    chai.request(TestHTTPService.connection)
      .put(`/api/v${rest.version}/users/1`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .send({
        username: '123'
      })
      .end(async (error, res) => {
        res.should.have.status(200);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: {
            id: 1,
            username: '123'
          }
        });

        done();
      });
  });

  it('Put user update without fields', (done) => {
    chai.request(TestHTTPService.connection)
      .put(`/api/v${rest.version}/users/1`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .send({})
      .end(async (error, res) => {
        res.should.have.status(415);
        res.should.to.be.json;
        res.body.should.be.eql(
        {
          errors: {
            username: ['Username can\'t be blank']
          }
        });

        done();
      });
  });

  it('Put user update if is not owner', (done) => {
    chai.request(TestHTTPService.connection)
      .put(`/api/v${rest.version}/users/2`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .send({})
      .end(async (error, res) => {
        res.should.have.status(403);
        res.should.to.be.json;
        res.body.should.be.eql(
        {
          errors: {
            user: ['Can\'t put (only owner)']
          }
        });

        done();
      });
  });
});
