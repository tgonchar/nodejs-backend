import chai from 'chai';
import chaiHttp from 'chai-http';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";
import models from "../src/models";
import http from "../config/http.json";

chai.use(chaiHttp);
chai.should();

describe(`GET /api/v${rest.version}/tasks/:id`, () => {

  beforeEach((done) => {
    let init = async () => {
      await TestHTTPService.start();

      await models.sequelize.drop();
      await models.sequelize.sync({ force: true });      

      let user1 = await models.user.build({
        username: 'user1',
        password: 'password1'
      }).save();
      let user2 = await models.user.build({
        username: 'user2',
        password: 'password2'
      }).save();
      let user3 = await models.user.build({
        username: 'user3',
        password: 'password3'
      }).save();

      let task1 = await models.task.create({
        title: 'Test task 1',
        userId: user1.id
      });
      await task1.addFollower(user2);
      await task1.addFollower(user3);

      done();
    }

    init();
  });

  afterEach((done) =>{
    models.sequelize.drop().then(() => {
      models.task.drop().then(() => {
        done();
      });
    });
  });

  it('Get task without token', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/tasks/1`)
      .end((error, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('Get task', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/tasks/1`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .end((error, res) => {
        res.should.have.status(200);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: {
            id: 1,
            title: 'Test task 1',
            userId: 1,
            followers: [
              {
                id: 2,
                username: 'user2'
              },
              {
                id: 3,
                username: 'user3'
              }
            ]
          }
        });
        done();
      });
  });
});
