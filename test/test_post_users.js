import chai from 'chai';
import chaiHttp from 'chai-http';
import Sequelize from 'sequelize';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";
import models from "../src/models";

chai.use(chaiHttp);
chai.should();

describe(`POST /api/v${rest.version}/users`, () => {

  beforeEach((done) => {
    let init = async () => {
      await TestHTTPService.start();

      await models.sequelize.drop();
      await models.sequelize.sync();
      done();
    }

    init();
  });

  afterEach((done) =>{
    models.sequelize.drop().then(() => {
      done();
    });
  });

  it('Simple signup', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/users`)
      .send({
        username: 'test',
        password: 'secret'
      })
      .end((error, res) => {
        res.should.have.status(201);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: {
            id: 1,
            username: 'test'
          }
        });

        models.user.findOne({
          where: {
            username: 'test'
          }
        }).then(user => {
          user.id.should.be.eql(1);
          user.username.should.be.eql('test');
          done();
        })
      });
  });

  it('Signup with duplicate user', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/users`)
      .send({
        username: 'uniq',
        password: 'some_secret'
      })
      .end((error, res) => {
        res.should.have.status(201);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: {
            id: 1,
            username: 'uniq'
          }
        });

        chai.request(TestHTTPService.connection)
          .post(`/api/v${rest.version}/users`)
          .send({
            username: 'uniq',
            password: 'some_secret'
          })
          .end((error, res) => {
            res.should.have.status(409);
            res.should.to.be.json;
            res.body.should.be.eql({
              errors: {
                user: ['User is exist']
              }
            });

            models.user.findOne({
              where: {
                username: 'uniq'
              }
            }).then(user => {
              user.id.should.be.eql(1);
              user.username.should.be.eql('uniq');
              done();
            })
          });
      });
  });

  it('Username and password length less 3', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/users`)
      .send({
        username: '11',
        password: '11'
      })
      .end((error, res) => {
        res.should.have.status(415);
        res.should.to.be.json;
        res.body.should.be.eql({
          errors: {
            password: ['Password length must be 3-20'],
            username: ['Username length must be 3-20']
          }
        });
        done();
      });
  });

  it('Username and password length more 20', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/users`)
      .send({
        username: '123456789012345678901',
        password: '123456789012345678901'
      })
      .end((error, res) => {
        res.should.have.status(415);
        res.should.to.be.json;
        res.body.should.be.eql({
          errors: {
            password: ['Password length must be 3-20'],
            username: ['Username length must be 3-20']
          }
        });
        done();
      });
  });

  it('Username not valid format', (done) => {
    chai.request(TestHTTPService.connection)
      .post(`/api/v${rest.version}/users`)
      .send({
        username: '*@qq',
        password: 'test'
      })
      .end((error, res) => {
        res.should.have.status(415);
        res.should.to.be.json;
        res.body.should.be.eql({
          errors: {
            username: [`Username Can't only a-z, A-Z, 0-9`]
          }
        });
        done();
      });
  });
});
