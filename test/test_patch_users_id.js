import chai from 'chai';
import chaiHttp from 'chai-http';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";
import models from "../src/models";
import http from "../config/http.json";

chai.use(chaiHttp);
chai.should();

describe(`PATCH /api/v${rest.version}/users/:id`, () => {

  beforeEach((done) => {
    let init = async () => {
      await TestHTTPService.start();

      await models.sequelize.drop();
      await models.sequelize.sync({ force: true });      

      let user1 = await models.user.build({
        username: 'user1',
        password: 'password1'
      }).save();

      done();
    }

    init();
  });

  afterEach((done) =>{
    models.sequelize.drop().then(() => {
      models.task.drop().then(() => {
        done();
      });
    });
  });

  it('Patch user without token', (done) => {
    chai.request(TestHTTPService.connection)
      .patch(`/api/v${rest.version}/users/1`)
      .end((error, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('Patch user', (done) => {
    chai.request(TestHTTPService.connection)
      .patch(`/api/v${rest.version}/users/1`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .send({
        username: 'test1',
        userId: 100,
        id: 100,
        followers: [{
          id: 2,
          username: 'user2'
        }]
      })
      .end(async (error, res) => {
        res.should.have.status(200);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: {
            id: 1,
            username: 'test1'
          }
        });

        let user = await models.user.findById(1);
        user.username.should.be.eql('test1');

        done();
      });
  });
});
