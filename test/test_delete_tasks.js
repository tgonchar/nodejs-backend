import chai from 'chai';
import chaiHttp from 'chai-http';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";
import models from "../src/models";
import http from "../config/http.json";

chai.use(chaiHttp);
chai.should();

describe(`DELETE /api/v${rest.version}/tasks/:id`, () => {

  beforeEach((done) => {
    let init = async () => {
      await TestHTTPService.start();

      await models.sequelize.drop();
      await models.sequelize.sync();

      let user1 = await models.user.create({
        username: 'user1',
        password: 'password1'
      });
      let user2 = await models.user.create({
        username: 'user2',
        password: 'password2'
      });

      let task1 = await models.task.create({
        title: 'Task 1',
        userId: user1.id
      });
      await task1.addFollower(user2);

      done();
    }

    init();
  });

  afterEach((done) =>{
    models.sequelize.drop().then(() => {
      done();
    });
  });

  it('Delete task without token', (done) => {
    chai.request(TestHTTPService.connection)
      .delete(`/api/v${rest.version}/tasks/1`)
      .end((error, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('Delete task', (done) => {
    chai.request(TestHTTPService.connection)
      .delete(`/api/v${rest.version}/tasks/1`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .end((error, res) => {
        res.should.have.status(200);

        models.task.findOne({
          where: {
            id: 1
          }
        }).then(async (task) => {
          (task == null).should.eql(true);
          done();
        })
      });
  });

  it('Delete task if not exist', (done) => {
    chai.request(TestHTTPService.connection)
      .delete(`/api/v${rest.version}/tasks/100`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .end((error, res) => {
        res.should.have.status(404);
        res.should.to.be.json;
        res.body.should.be.eql({
          errors: {
            task: [`Can't find`]
          }
        });

        models.task.findOne({
          where: {
            id: 1
          }
        }).then(async (task) => {
          task.id.should.be.eql(1);
          task.title.should.be.eql('Task 1');
          task.userId.should.be.eql(1);
          done();
        })
      });
  });
});
