import chai from 'chai';
import chaiHttp from 'chai-http';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';

import TestHTTPService from './utils/http_service';
import rest from "../config/rest.json";
import models from "../src/models";
import http from "../config/http.json";

chai.use(chaiHttp);
chai.should();

describe(`GET /api/v${rest.version}/users`, () => {

  beforeEach((done) => {
    let init = async () => {
      await TestHTTPService.start();

      await models.sequelize.drop();
      await models.sequelize.sync();

      await models.user.build({
        username: 'user1',
        password: 'password1'
      }).save();
      await models.user.build({
        username: 'user2',
        password: 'password2'
      }).save();
      await models.user.build({
        username: 'user3',
        password: 'password3'
      }).save();

      done();
    }

    init();
  });

  afterEach((done) =>{
    models.sequelize.drop().then(() => {
      done();
    });
  });

  it('Get users list without token', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/users`)
      .end((error, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('Get users list without offset and limit', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/users`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .end((error, res) => {
        res.should.have.status(200);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: [
            {
              id: 1,
              username: 'user1'
            },
            {
              id: 2,
              username: 'user2'
            },
            {
              id: 3,
              username: 'user3'
            }
          ]
        });
        done();
      });
  });

  it('Get users list with offset and limit', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/users`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .send({
        offset: 0,
        limit: 2
      })
      .end((error, res) => {
        res.should.have.status(200);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: [
            {
              id: 1,
              username: 'user1'
            },
            {
              id: 2,
              username: 'user2'
            }
          ]
        });
        
        chai.request(TestHTTPService.connection)
          .get(`/api/v${rest.version}/users`)
          .set({
            token: jwt.sign({
              id: 1,
              username: 'user1'
            }, http.token.secret, {
              expiresIn: http.token.expires
            })
          })
          .send({
            offset: 2,
            limit: 2
          })
          .end((error, res) => {
            res.should.have.status(200);
            res.should.to.be.json;
            res.body.should.be.eql({
              data: [
                {
                  id: 3,
                  username: 'user3'
                }
              ]
            });
            done();
          });
      });
  });

  it('Get users list with large offset', (done) => {
    chai.request(TestHTTPService.connection)
      .get(`/api/v${rest.version}/users`)
      .set({
        token: jwt.sign({
          id: 1,
          username: 'user1'
        }, http.token.secret, {
          expiresIn: http.token.expires
        })
      })
      .send({
        offset: 100,
        limit: 2
      })
      .end((error, res) => {
        res.should.have.status(200);
        res.should.to.be.json;
        res.body.should.be.eql({
          data: []
        });
        done();
      });
  });
});
