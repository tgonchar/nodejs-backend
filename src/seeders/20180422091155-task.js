'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('tasks', [
      {
        title: 'Choose a pill',
        createdAt: new Date(),
        updatedAt: new Date(),
        userId: 2
      },
      {
        title: 'Stop the MATRIX',
        createdAt: new Date(),
        updatedAt: new Date(),
        userId: 2
      },
      {
        title: 'Visit to oracle',
        createdAt: new Date(),
        updatedAt: new Date(),
        userId: 2
      },
      {
        title: 'Add some tasks',
        createdAt: new Date(),
        updatedAt: new Date(),
        userId: 1
      },
      {
        title: 'Fall in love with Neo',
        createdAt: new Date(),
        updatedAt: new Date(),
        userId: 4
      }
    ], {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('tasks', null, {});
  }
};
