'use strict';
const crypto = require('crypto');

module.exports = {
  up: (queryInterface) => {
    let salt = Math.round((new Date().valueOf() * Math.random())) + '';

    return queryInterface.bulkInsert('users', [
      {
        id: 1,
        username: 'admin',
        salt: salt,
        hash_password: crypto.createHmac('sha1', salt).update('admin').digest('hex'),
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 2,
        username: 'neo',
        salt: salt,
        hash_password: crypto.createHmac('sha1', salt).update('neo').digest('hex'),
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 3,
        username: 'morpheus',
        salt: salt,
        hash_password: crypto.createHmac('sha1', salt).update('morpheus').digest('hex'),
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 4,
        username: 'trinity',
        salt: salt,
        hash_password: crypto.createHmac('sha1', salt).update('trinity').digest('hex'),
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('users', null, {});
  }
};
