'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('follower_task', [
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        taskId: 1,
        userId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        taskId: 2,
        userId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        taskId: 2,
        userId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        taskId: 3,
        userId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        taskId: 3,
        userId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        taskId: 4,
        userId: 4
      }
    ], {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('tasks', null, {});
  }
};
