export const titleConstraints = {
  presence: true
}

export const userIdConstraints = {
  presence: true,
  numericality: {
    onlyInteger: true,
    greaterThan: 0
  }
}
