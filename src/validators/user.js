export const usernameConstraints = {
  presence: true,
  length: {
    minimum: 3,
    maximum: 20,
    message: "length must be 3-20"
  },
  format: {
    pattern: "^[a-z0-9]*$",
    message: "Can't only a-z, A-Z, 0-9"
  }
}

export const passwordConstraints = {
  presence: true,
  length: {
    minimum: 3,
    maximum: 20,
    message: "length must be 3-20"
  }
}