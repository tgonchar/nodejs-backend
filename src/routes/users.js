import { Router } from 'express';
import passport from 'passport';
import validate from 'validate.js';
import jwt from 'jsonwebtoken';

import http from '../../config/http.json';
import models from '../models';
import { usernameConstraints, passwordConstraints } from '../validators/user';

const router = Router();

/**
 * @api {get} /api/v1.0/users/current Current
 * @apiDescription Получить текущего пользователя по токену. Токен необходимо передавать в заголовке (header)
 * @apiVersion 1.0.0
 * @apiName current
 * @apiGroup Users
 * @apiPermission all
 *
 * @apiHeader {String} token Токен, полученный при аутентификации
 *
 * @apiSuccessExample Success:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": {
 *       "id": 1,
 *       "username": "test"
 *     }
 *   }
 *
 * @apiErrorExample Fail token:
 *   HTTP/1.1 401 Unauthorized
 */
router.get("/current", passport.authenticate('jwt', { session: false }), function(req, res){
  res.json({
    data: {
      id: req.user.id,
      username: req.user.username
    }
  });
});

/**
 * @api {post} /api/v1.0/users Add user
 * @apiDescription Добавление нового пользователя (регистрация)
 * @apiVersion 1.0.0
 * @apiName sign_up
 * @apiGroup Users
 * @apiPermission all
 *
 * @apiParam {String} username Имя пользователя (3-20 символов 0-9/a-z/A-Z)
 * @apiParam {String} password Пароль пользователя (3-20 любых символов)
 *
 * @apiSuccessExample SUCCESS:
 *   HTTP/1.1 201 Created
 *   {
 *     "data": {
 *       "id": 1,
 *       "username": "neo"
 *     }
 *   }
 *
 * @apiErrorExample Duplicate:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "errors": {
 *       "user": ["User is exist"]
 *     }
 *   }
 * @apiErrorExample Validation fail:
 *   HTTP/1.1 415 Unsupported Media Type
 *   {
 *     "errors": {
 *       "username": ["Username Can't only a-z, A-Z, 0-9"],
 *       "password": ["Password length must be 3-20"],
 *     }
 *   }
 */
router.post("/", async (req, res) => {
  const validationResult = validate(req.body, {
    username: usernameConstraints,
    password: passwordConstraints,
  });

  if (validationResult) {
    res.status(415).json({
      errors: validationResult
    });
  } else {
    const user = await models.user.findOne({
      where: {
        username: req.body.username
      }
    })
    
    if (user) {
      res.status(409).json({
        errors: {
          user: [`User is exist`]
        }
      });
    }
    else {
      let newUser = models.user.build({
        username: req.body.username,
        password: req.body.password
      });

      let createdUser = await newUser.save();
      if (createdUser) {
        res.status(201).json({
          data: {
            id: createdUser.id,
            username: createdUser.username
          }
        });
      } else {
        res.status(409).json({
          errors: {
            user: [`User is not saved`]
          }
        });
      }
    }
  }
});

/**
 * @api {get} /api/v1.0/users Users list
 * @apiDescription Получение списка пользователей
 * @apiVersion 1.0.0
 * @apiName users_list
 * @apiGroup Users
 * @apiPermission all
 *
 * @apiHeader {String} token Токен, полученный при аутентификации
 *
 * @apiParam {String} offset Смещение относительно первого пользователя, по умолчанию 0
 * @apiParam {String} limit Максимальное количество пользователей в ответе, по умолчанию 10
 *
 * @apiSuccessExample SUCCESS:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": [
 *       {
 *         "id": 1,
 *         "username": "neo"
 *       },
 *       {
 *         "id": 2,
 *         "username": "trinity"
 *       }
 *     ]
 *   }
 *
 * @apiErrorExample Without token:
 *   HTTP/1.1 401 Unauthorized
 */
router.get("/", passport.authenticate('jwt', { session: false }), async (req, res) => {
  const offset = req.body.offset || 0;
  const limit = req.body.limit || 10;

  try {
    let users = await models.user.findAll({ offset, limit});

    if (users) {
      res.json({
        data: users.map(user => {
          return {
            id: user.id,
            username: user.username
          }
        })
      })
    }
    else {
      throw Error('Users not found');
    }
  } catch (error) {
    console.log(error);

    res.status(409).json({
      errors: {
        users: [`Can't get users list`]
      }
    });
  }
});

/**
 * @api {get} /api/v1.0/users/:id Get user
 * @apiDescription Получение пользователя по id
 * @apiVersion 1.0.0
 * @apiName get
 * @apiGroup Users
 * @apiPermission all
 *
 * @apiHeader {String} token Токен, полученный при аутентификации
 *
 * @apiParam {Number} id Идентификатор пользователя
 *
 * @apiSuccessExample SUCCESS:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": {
 *       id: 1,
 *       username: 'user1'
 *     }
 *   }
 *
 * @apiErrorExample Without token:
 *   HTTP/1.1 401 Unauthorized
 * @apiErrorExample Not found:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "errors": {
 *       "user": ["Can't get user"]
 *     }
 *   }
 */
router.get("/:id", passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    let user = await models.user.findById(req.params.id);

    if (user) {
      res.json({
        data: {
          id: user.id,
          username: user.username
        }
      });
    } else {
      res.status(404).json({
        errors: {
          user: [`Can't find`]
        }
      });
    }
  } catch (error) {
    console.log(error);

    res.status(409).json({
      errors: {
        user: [`Can't get user`]
      }
    });
  }
});

/**
 * @api {post} /api/v1.0/users/sign_in SignIn
 * @apiDescription Аутентификация нового пользователя
 * @apiVersion 1.0.0
 * @apiName sign_in
 * @apiGroup Users
 * @apiPermission all
 *
 * @apiParam {String} username Имя пользователя (3-20 символов 0-9/a-z/A-Z)
 * @apiParam {String} password Пароль пользователя (3-20 любых символов)
 *
 * @apiSuccessExample Success:
 *   HTTP/1.1 200 OK
 *   {
 *     "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJhZG1pbiIsImlhdCI6MTUyMzIyMDg1NywiZXhwIjoxNTIzMzA3MjU3fQ.Zaf3Agp-NkUFM9Pu-ACb7KP6o8bIb_R9-5TZ3lheDXc"
 *   }
 *
 * @apiErrorExample Fail auth:
 *   HTTP/1.1 401 Unauthorized
 *   {
 *     "errors": {
 *       user: ['Authenticate is failed']
 *     }
 *   }
 * @apiErrorExample Validation fail:
 *   HTTP/1.1 415 Unsupported Media Type
 *   {
 *     "errors": {
 *       "username": ["Username Can't only a-z, A-Z, 0-9"],
 *       "password": ["Password length must be 3-20"],
 *     }
 *   }
 */
router.post("/sign_in", (req, res) => {
  const validationResult = validate(req.body, {
    username: usernameConstraints,
    password: passwordConstraints,
  });

  if (validationResult) {
    res.status(415).json({
      errors: validationResult
    });
  } else {
    passport.authenticate('local', {session: false}, (err, user) => {
      if (err || !user) {
        return res.status(401).json({
          errors: {
            user: [`Authenticate is failed`]
          }
        });
      }

      req.login(user, {session: false}, (err) => {
        if (err) {
          res.status(401).json({
            errors: {
              user: [`Authenticate is failed`]
            }
          });
        }

        let token = jwt.sign({
          id: user.id,
          username: user.username,
        }, http.token.secret, {
          expiresIn: http.token.expires
        });


        return res.json({
          token: token
        });
      });
    })(req, res);
  }
});

/**
 * @api {delete} /api/v1.0/users/:id Delete user
 * @apiDescription Удаление пользователя
 * @apiVersion 1.0.0
 * @apiName delete
 * @apiGroup Users
 * @apiPermission all
 *
 * @apiHeader {String} token Токен, полученный при аутентификации
 *
 * @apiParam {Number} id Идентификатор пользователя
 *
 * @apiSuccessExample SUCCESS:
 *   HTTP/1.1 200 OK
 *   {}
 *
 * @apiErrorExample Without token:
 *   HTTP/1.1 401 Unauthorized
 * @apiErrorExample Not found:
 *   HTTP/1.1 404 Not found
 *   {
 *     "errors": {
 *       "user": ["Can't find"]
 *     }
 *   }
 * @apiErrorExample Not found:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "errors": {
 *       "user": ["Can't delete (only owner)"]
 *     }
 *   }
 */
router.delete("/:id", passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    if (req.params.id != req.user.id) {
      res.status(403).json({
        errors: {
          user: [`Can't delete (only owner)`]
        }
      });
      return;
    }

    let user = await models.user.findById(req.params.id);

    if (user) {
      await user.destroy();
      res.sendStatus(200);
    } else {
      res.status(404).json({
        errors: {
          user: [`Can't find`]
        }
      });
    }
  } catch (error) {
    console.log(error);

    res.status(409).json({
      errors: {
        user: [`Can't delete user`]
      }
    });
  }
});

/**
 * @api {put} /api/v1.0/users/:id Update user
 * @apiDescription Обновление пользователя по id
 * @apiVersion 1.0.0
 * @apiName put
 * @apiGroup Users
 * @apiPermission all
 *
 * @apiHeader {String} token Токен, полученный при аутентификации
 *
 * @apiParam {Number} id Идентификатор задачи
 * @apiParam {String} username Имя пользователя
 *
 * @apiSuccessExample Update:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": {
 *       "id": 1,
 *       "username": "user1"
 *     }
 *   }
 *
 * @apiErrorExample Without token:
 *   HTTP/1.1 401 Unauthorized
 * @apiErrorExample Not found:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "errors": {
 *       "task": ["Can't put user"]
 *     }
 *   }
 * @apiErrorExample Validation fail:
 *   HTTP/1.1 415 Unsupported Media Type
 *   {
 *     "errors": {
 *       "username": ["Username can't be blank"]
 *     }
 *   }
 * @apiErrorExample Forbidden:
 *   HTTP/1.1 403 Forbidden
 *   {
 *     "errors": {
 *       "user": ["Can\'t put (only owner)"]
 *     }
 *   }
 */
router.put("/:id", passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (req.params.id != req.user.id) {
    res.status(403).json({
      errors: {
        user: [`Can't put (only owner)`]
      }
    });
    return;
  }

  const validationResult = validate(req.body, {
    username: usernameConstraints
  });

  if (validationResult) {
    res.status(415).json({
      errors: validationResult
    });
  } else {
    try {
      let user = await models.user.findById(req.params.id);
      user.username = req.body.username;

      res.status(200).json({
        data: {
          id: user.id,
          username: user.username
        }
      });
    } catch (error) {
      console.log(error);

      res.status(409).json({
        errors: {
          user: [`Can't put user`]
        }
      });
    }
  }
});

/**
 * @api {patch} /api/v1.0/users/:id Update user by part
 * @apiDescription Частичное обновление пользователя по id
 * @apiVersion 1.0.0
 * @apiName patch
 * @apiGroup Users
 * @apiPermission all
 *
 * @apiHeader {String} token Токен, полученный при аутентификации
 *
 * @apiParam {String} title Имя пользователя (не обязательно)
 *
 * @apiSuccessExample Update:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": {
 *       "id": 1,
 *       "username": 'user1'
 *     }
 *   }
 *
 * @apiErrorExample Without token:
 *   HTTP/1.1 401 Unauthorized
 * @apiErrorExample Not found:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "errors": {
 *       "user": ["Can't patch user"]
 *     }
 *   }
 */
router.patch("/:id", passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    let requireFields = ['username'];
    let data = {};

    requireFields.forEach(field => {
      if (req.body[field]) {
        data[field] = req.body[field];
      }
    });

    await req.user.update(data);

    res.json({
      data: {
        id: req.user.id,
        username: req.user.username
      }
    });

  } catch (error) {
    console.log(error);

    res.status(409).json({
      errors: {
        user: [`Can't patch user`]
      }
    });
  }
});

export default router;