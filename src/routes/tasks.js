import { Router } from 'express';
import passport from 'passport';
import validate from 'validate.js';

import models from '../models';
import { titleConstraints } from '../validators/task';

const router = Router();

/**
 * @api {get} /api/v1.0/tasks/share Shared tasks list
 * @apiDescription Получение списка задач расшаренных пользователю
 * @apiVersion 1.0.0
 * @apiName shared_tasks_list
 * @apiGroup Tasks
 * @apiPermission all
 *
 * @apiHeader {String} token Токен, полученный при аутентификации
 *
 * @apiParam {String} offset Смещение относительно первой задачи, по умолчанию 0
 * @apiParam {String} limit Максимальное количество задач в ответе, по умолчанию 10
 *
 * @apiSuccessExample SUCCESS:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": [
 *       {
 *         "id: 1,
 *         "title": "Test task 1",
 *         "userId": 1,
 *         "followers": [
 *           {
 *             "id": 2,
 *             "username": "user2"
 *           },
 *           {
 *             "id": 3,
 *             "username": "user3"
 *           }
 *         ]
 *       }
 *     ]
 *   }
 *
 * @apiErrorExample Without token:
 *   HTTP/1.1 401 Unauthorized
 */
router.get("/share", passport.authenticate('jwt', { session: false }), async (req, res) => {
  const offset = req.body.offset || 0;
  const limit = req.body.limit || 10;

  try {
    let tasks = await models.follower_task.findAll({
      where: {
        userId: req.user.id,
      },
      offset,
      limit
    });

    let result = [];
    await Promise.all(tasks.map(async (taskFollower) => {
      let task = await models.task.findById(taskFollower.taskId);
      if(task) {
        result.push({
          id: task.id,
          userId: task.userId,
          title: task.title,
          followers: await Promise.all(await task.getFollower().map((user) => {
            return {
              id: user.id,
              username: user.username
            }
          }))
        });
      }
    }));

    res.json({
      data: result
    });

  } catch (error) {
    console.log(error);

    res.status(409).json({
      errors: {
        tasks: [`Can't get shared tasks list`]
      }
    });
  }
});

/**
 * @api {get} /api/v1.0/tasks Tasks list
 * @apiDescription Получение списка задач пользователя
 * @apiVersion 1.0.0
 * @apiName tasks_list
 * @apiGroup Tasks
 * @apiPermission all
 *
 * @apiHeader {String} token Токен, полученный при аутентификации
 *
 * @apiParam {String} offset Смещение относительно первой задачи, по умолчанию 0
 * @apiParam {String} limit Максимальное количество задач в ответе, по умолчанию 10
 *
 * @apiSuccessExample SUCCESS:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": [
 *       {
 *         "id": 1,
 *         "title": "Task 1"
 *         "followers": [
 *           {
 *             "id": 1,
 *             "username": "user"
 *           }
 *         ]
 *       }
 *     ]
 *   }
 *
 * @apiErrorExample Without token:
 *   HTTP/1.1 401 Unauthorized
 */
router.get("/", passport.authenticate('jwt', { session: false }), async (req, res) => {
  const offset = req.body.offset || 0;
  const limit = req.body.limit || 10;

  try {
    let user = await models.user.findOne({
      where: {
        id: req.user.id
      },
      include: [
        {
          model: models.task,
          as: 'tasks',
          offset,
          limit
        }
      ]
    });

    if (user) {
      res.json({
        data: await Promise.all(user.tasks.map(async (task) => {
          return {
            id: task.id,
            title: task.title,
            userId: task.userId,
            followers: await Promise.all(await task.getFollower().map((user) => {
              return {
                id: user.id,
                username: user.username
              }
            }))
          }
        }))
      });
    }
    else {
      throw Error('Users not found');
    }
  } catch (error) {
    console.log(error);

    res.status(409).json({
      errors: {
        tasks: [`Can't get tasks list`]
      }
    });
  }
});

/**
 * @api {get} /api/v1.0/tasks/:id Get task
 * @apiDescription Получение задачи по id
 * @apiVersion 1.0.0
 * @apiName get
 * @apiGroup Tasks
 * @apiPermission all
 *
 * @apiHeader {String} token Токен, полученный при аутентификации
 *
 * @apiParam {Number} id Идентификатор задачи
 * @apiParam {String} title Заголовок задачи
 * @apiParam {Array} followers Масив подписчиков, где указан id пользователя и username
 *
 * @apiSuccessExample SUCCESS:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": {
 *       "id": 1,
 *       "title": "Test task 1",
 *       "userId": 1,
 *       "followers": [
 *         {
 *           "id": 2,
 *           "username": "user2"
 *         }
 *       ]
 *     }
 *   }
 *
 * @apiErrorExample Without token:
 *   HTTP/1.1 401 Unauthorized
 * @apiErrorExample Not found:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "errors": {
 *       "task": ["Can't get task"]
 *     }
 *   }
 */
router.get("/:id", passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    let task = await models.task.findById(req.params.id);

    if (task) {
      res.json({
        data: {
          id: task.id,
          title: task.title,
          userId: task.userId,
          followers: await Promise.all(await task.getFollower().map((user) => {
            return {
              id: user.id,
              username: user.username
            }
          }))
        }
      });
    } else {
      res.status(404).json({
        errors: {
          task: [`Can't find`]
        }
      });
    }
  } catch (error) {
    console.log(error);

    res.status(409).json({
      errors: {
        task: [`Can't get task`]
      }
    });
  }
});

/**
 * @api {post} /api/v1.0/tasks Add task
 * @apiDescription Добавление новой задачи
 * @apiVersion 1.0.0
 * @apiName add
 * @apiGroup Tasks
 * @apiPermission all
 *
 * @apiHeader {String} token Токен, полученный при аутентификации
 *
 * @apiParam {String} title Заголовок задачи
 * @apiParam {Array} followers Масив подписчиков, где указан id пользователя и username
 *
 * @apiSuccessExample SUCCESS:
 *   HTTP/1.1 201 Created
 *   {
 *     "data": {
 *       "id": 1,
 *       "title": "Task 1"
 *       "followers": [
 *         {
 *           "id": 1,
 *           "username": "user"
 *         }
 *       ]
 *     }
 *   }
 *
 * @apiErrorExample Without token:
 *   HTTP/1.1 401 Unauthorized
 * @apiErrorExample Validation fail:
 *   HTTP/1.1 415 Unsupported Media Type
 *   {
 *     "errors": {
 *       "title": ["Title can't be blank"]
 *     }
 *   }
 */
router.post("/", passport.authenticate('jwt', { session: false }), async (req, res) => {
  const validationResult = validate(req.body, {
    title: titleConstraints
  });

  if (validationResult) {
    res.status(415).json({
      errors: validationResult
    });
  } else {
    try {
      let newTask = await models.task.create({
        title: req.body.title,
        userId: req.user.id
      });

      if (req.body.followers) {
        for (let index = 0; index < req.body.followers.length; index++) {
          let follower = req.body.followers[index];

          let user = await models.user.findOne({
            where: {
              id: follower.id,
              username: follower.username
            }
          });

          if (user) {
            await newTask.addFollower(user);
          }
        }
      }

      res.status(201).json({
        data: {
          id: newTask.id,
          title: newTask.title,
          followers: await Promise.all(await newTask.getFollower().map((user) => {
            return {
              id: user.id,
              username: user.username
            }
          }))
        }
      });
    } catch (error) {
      console.log(error);

      res.status(409).json({
        errors: {
          task: [`Can't create task`]
        }
      });
    }
  }
});

/**
 * @api {put} /api/v1.0/tasks/:id Update task
 * @apiDescription Обновление задачи по id
 * @apiVersion 1.0.0
 * @apiName put
 * @apiGroup Tasks
 * @apiPermission all
 *
 * @apiHeader {String} token Токен, полученный при аутентификации
 *
 * @apiParam {Number} id Идентификатор задачи
 * @apiParam {String} title Заголовок задачи
 * @apiParam {Array} followers Масив подписчиков, где указан id пользователя и username
 *
 * @apiSuccessExample Update:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": {
 *       "id": 1,
 *       "title": "Test task 1",
 *       "userId": 1,
 *       "followers": [
 *         {
 *           "id": 2,
 *           "username": "user2"
 *         }
 *       ]
 *     }
 *   }
 *
 * @apiSuccessExample Create:
 *   HTTP/1.1 201 Created
 *   {
 *     "data": {
 *       "id": 1,
 *       "title": "Test task 1",
 *       "userId": 1,
 *       "followers": [
 *         {
 *           "id": 2,
 *           "username": "user2"
 *         }
 *       ]
 *     }
 *   }
 *
 * @apiErrorExample Without token:
 *   HTTP/1.1 401 Unauthorized
 * @apiErrorExample Not found:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "errors": {
 *       "task": ["Can't put task"]
 *     }
 *   }
 * @apiErrorExample Validation fail:
 *   HTTP/1.1 415 Unsupported Media Type
 *   {
 *     "errors": {
 *       "title": ["Title can't be blank"]
 *     }
 *   }
 */
router.put("/:id", passport.authenticate('jwt', { session: false }), async (req, res) => {
  const validationResult = validate(req.body, {
    title: titleConstraints
  });

  if (validationResult) {
    res.status(415).json({
      errors: validationResult
    });
  } else {
    try {
      let task = await models.task.findOne({
        where: {
          id: req.params.id,
          userId: req.user.id
        }
      });
      let isCreate = false;

      if (!task) {
        task = await models.task.create({
          id: req.params.id,
          title: req.body.title,
          userId: req.user.id
        });
        isCreate = true;
      }

      task.title = req.body.title;
      task.userId = req.user.id;
      await task.save();
      await task.setFollower([]);

      if (req.body.followers) {
        for (let index = 0; index < req.body.followers.length; index++) {
          let follower = req.body.followers[index];

          let user = await models.user.findOne({
            where: {
              id: follower.id,
              username: follower.username
            }
          });

          if (user) {
            await task.addFollower(user);
          }
        }
      }

      res.status(isCreate ? 201 : 200).json({
        data: {
          id: task.id,
          title: task.title,
          userId: task.userId,
          followers: await Promise.all(await task.getFollower().map((user) => {
            return {
              id: user.id,
              username: user.username
            }
          }))
        }
      });
    } catch (error) {
      console.log(error);

      res.status(409).json({
        errors: {
          task: [`Can't put task`]
        }
      });
    }
  }
});

/**
 * @api {patch} /api/v1.0/tasks/:id Update task by part
 * @apiDescription Частичное бновление задачи по id
 * @apiVersion 1.0.0
 * @apiName patch
 * @apiGroup Tasks
 * @apiPermission all
 *
 * @apiHeader {String} token Токен, полученный при аутентификации
 *
 * @apiParam {Number} id Идентификатор задачи
 * @apiParam {String} title Заголовок задачи (не обязательно)
 * @apiParam {Array} followers Масив подписчиков, где указан id пользователя и username (не обязательно)
 *
 * @apiSuccessExample Update:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": {
 *       "id": 1,
 *       "title": "Test task 1",
 *       "userId": 1,
 *       "followers": [
 *         {
 *           "id": 2,
 *           "username": "user2"
 *         }
 *       ]
 *     }
 *   }
 *
 * @apiErrorExample Without token:
 *   HTTP/1.1 401 Unauthorized
 * @apiErrorExample Not found:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "errors": {
 *       "task": ["Can't find"]
 *     }
 *   }
 */
router.patch("/:id", passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    let requireFields = ['title'];
    let data = {};

    requireFields.forEach(field => {
      if (req.body[field]) {
        data[field] = req.body[field];
      }
    });

    let task = await models.task.findOne({
      where: {
        id: req.params.id,
        userId: req.user.id
      }
    });

    if (task) {
      await task.update(data);
      await task.setFollower([]);

      if (req.body.followers) {
        for (let index = 0; index < req.body.followers.length; index++) {
          let follower = req.body.followers[index];

          let user = await models.user.findOne({
            where: {
              id: follower.id,
              username: follower.username
            }
          });

          if (user) {
            await task.addFollower(user);
          }
        }
      }

      res.json({
        data: {
          id: task.id,
          title: task.title,
          userId: task.userId,
          followers: await Promise.all(await task.getFollower().map((user) => {
            return {
              id: user.id,
              username: user.username
            }
          }))
        }
      });
    } else {
      res.status(404).json({
        errors: [
          {
            task: `Can't find`
          }
        ]
      });
    }

  } catch (error) {
    console.log(error);

    res.status(409).json({
      errors: {
        task: [`Can't patch task`]
      }
    });
  }
});

/**
 * @api {delete} /api/v1.0/tasks/:id Delete task
 * @apiDescription Удаление новой задачи
 * @apiVersion 1.0.0
 * @apiName delete
 * @apiGroup Tasks
 * @apiPermission all
 *
 * @apiHeader {String} token Токен, полученный при аутентификации
 *
 * @apiParam {Number} id Идентификатор задачи
 *
 * @apiSuccessExample SUCCESS:
 *   HTTP/1.1 200 OK
 *   {}
 *
 * @apiErrorExample Without token:
 *   HTTP/1.1 401 Unauthorized
 * @apiErrorExample Not found:
 *   HTTP/1.1 404 Not found
 *   {
 *     "errors": {
 *       "task": ["Can't find"]
 *     }
 *   }
 */
router.delete("/:id", passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    let task = await models.task.findOne({
      where: {
        id: req.params.id,
        userId: req.user.id
      }
    });

    if (task) {
      await task.destroy();
      res.sendStatus(200);
    } else {
      res.status(404).json({
        errors: {
          task: [`Can't find`]
        }
      });
    }
  } catch (error) {
    console.log(error);

    res.status(409).json({
      errors: {
        task: [`Can't delete task`]
      }
    });
  }
});

export default router;