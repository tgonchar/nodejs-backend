import { Router } from 'express';

import rest from "../../config/rest.json";

const router = Router();

/**
 * @api {get} /api Get API information
 * @apiDescription Получение информации об API
 * @apiVersion 1.0.0
 * @apiName version
 * @apiGroup API
 * @apiPermission all
 *
 * @apiSuccessExample SUCCESS:
 *   HTTP/1.1 200 OK
 *   {
 *     "version": "1.0"
 *   }
 */
router.get("/", (req, res) => {
  res.json({
    data: {
      version: rest.version
    }
  });
});

export default router;