import { Router } from 'express';

import api from "./api";
import users from "./users";
import tasks from "./tasks";
import rest from "../../config/rest.json";

const router = Router();

router.use(api);
router.use(`/v${rest.version}/users/`, users);
router.use(`/v${rest.version}/tasks/`, tasks);

export default router;