import HTTPService from "./http/service";
import http from "../config/http.json";
import models from "../src/models";

models.sequelize.sync().then(() => {
  HTTPService.start(process.env.PORT || http.port);
}).catch(error => {
  console.log(error);
});