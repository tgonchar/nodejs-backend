'use strict';
module.exports = (sequelize, DataTypes) => {
  var task = sequelize.define('task', {
    title: {
      type: DataTypes.STRING,
      notNull: true
    }
  }, {});

  task.associate = function(models) {
    task.belongsTo(models.user, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
    task.belongsToMany(models.user, {
      as: 'follower',
      through: 'follower_task',
      foreignKey: 'taskId'
    });
  };

  return task;
};