'use strict';

module.exports = (sequelize, DataTypes) => {
  var followerTask = sequelize.define('follower_task', {
    userId: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    taskId: {
      type: DataTypes.INTEGER,
      primaryKey: true
    }
  }, {
    tableName: 'follower_task'
  });

  followerTask.associate = function() {
  };

  return followerTask;
};
