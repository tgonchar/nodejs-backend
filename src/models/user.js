'use strict';
import crypto from 'crypto';

module.exports = (sequelize, DataTypes) => {
  var user = sequelize.define('user', {
    username: {
      type: DataTypes.STRING,
      unique: true,
      notNull: true,
      validate: {
        is: /^[a-z0-9]*$/,
        len: [3,20]
      }
    },
    salt: {
      type: DataTypes.STRING,
      notNull: true
    },
    hash_password: {
      type: DataTypes.STRING,
      notNull: true
    }
  }, {
    setterMethods: {
      password(value) {
        let salt = Math.round((new Date().valueOf() * Math.random())) + '';
        this.setDataValue('salt', salt);

        try {
          let hash_password =  crypto
            .createHmac('sha1', salt)
            .update(value)
            .digest('hex');

          this.setDataValue('hash_password', hash_password);          
        } catch (error) {
          console.log(error)
        }
      }
    }
  });

  user.associate = function(models) {
    user.hasMany(models.task, {
      as: 'tasks'
    });
    user.belongsToMany(models.task, {
      as: 'sharedTasks',
      through: 'follower_task',
      foreignKey: 'userId'
    });
  };

  user.prototype.authenticate = function (password) {
    let hash_password =  crypto
      .createHmac('sha1', this.getDataValue('salt'))
      .update(password)
      .digest('hex');

    return hash_password === this.getDataValue('hash_password');
  }

  return user;
};
