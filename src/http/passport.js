import passport from 'passport';
import passportJWT from 'passport-jwt';
import { Strategy } from 'passport-local';

import models from '../models';
import http from '../../config/http.json';

class PasportService {
  use(application) {
    passport.use(new Strategy((username, password, done) => {
      models.user.findOne({
        where: {
        username
      }}).then(user => {
        if (!user) {
          return done(new Error('User not found'));
        }

        if (user.authenticate(password)) {
          done(null, user);
        } else {
          return done(null, false, {message: 'Incorrect user'});
        }
      }).catch(() => {
        return done(null, false, {message: 'Incorrect user'});
      });
    }));

    passport.use(new passportJWT.Strategy({
      jwtFromRequest: passportJWT.ExtractJwt.fromHeader('token'),
      secretOrKey: http.token.secret  
    }, (payload, cb) => {
      return models.user.findOne({
          where: {
          id: payload.id
        }}).then(user => {
          return cb(null, user);
        }).catch((error) => {
          return cb(error);
        });
      }
    ));

    application.use(passport.initialize());
  }
}

export default new PasportService();