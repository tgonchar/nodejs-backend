import express from "express";
import bodyParser from "body-parser";

import routes from '../routes/index';
import PasportService from './passport';

const application = express();
var connection;

class HTTPService {
  constructor() {
    application.use(bodyParser.json());
    application.use("/api/", routes);
    PasportService.use(application);
  }

  async start (port) {
    let expressPromise = new Promise((resolve) => {
      connection = application.listen(port, function () {
        console.log(`Listenning on ${port}`);
        resolve();
      });
    });

    try {
      await expressPromise;
      return connection;
    } catch (error) {
      console.log(error);
    }
  }
}

export default new HTTPService();