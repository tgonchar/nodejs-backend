'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      username: {
        type: Sequelize.STRING,
        notNull: true,
        unique: true,
        validate: {
          is: /^[a-z0-9]*$/,
          len: [3,20]
        }
      },
      salt: {
        type: Sequelize.STRING,
        notNull: true
      },
      hash_password: {
        type: Sequelize.STRING,
        notNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('users');
  }
};