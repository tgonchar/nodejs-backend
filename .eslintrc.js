module.exports = {
  parser: "babel-eslint",
  "env": {
    "browser": true,
    "node": true,
    "es6": true
  },
  root: true,
  extends: "eslint:recommended",
  "rules": {
    "no-console": "off"
  }
}