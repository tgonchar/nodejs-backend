var process = require("process");

module.exports = {
  development: {
    username: "admin",
    password: "31415",
    database: "tasks_dev",
    host: "localhost",
    dialect: "postgres",
    // logging: false
  },
  test: { 
    username: process.env.DB_USER || "admin",
    password: process.env.DB_PASSWORD || "31415",
    database: process.env.DB_NAME || "tasks_test",
    host: process.env.DB_HOST || "localhost",
    logging: false,
    dialect: "postgres"
  },
  production: {
    username:  process.env.DB_USER || "admin",
    password: process.env.DB_PASSWORD || "31415",
    database: process.env.DB_NAME || "tasks",
    host: process.env.DB_HOST || "localhost",
    dialect: "postgres",
    logging: false
  }
}
