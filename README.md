## Express backend

Example NodeJS backend with REST API (Express)

##### REQUIREMENTS

Для работы backend'a необходимо наличие подключения к базе данных. Настройки базы данных вы можете найти в соответствующих разделах файла конфигурации `/config/db.js` (обратите внимание, что конфигурации отличны и для проведения тестирования или разработки, необходимо создать соответсвующие конфигурации базы данных). Необходимой версией NodeJS является 8.11.

Установка зависимостей:
`bash
yarn
`

Настройка базы данных
`bash
sudo -u postgres psql postgres
create database tasks;
create database tasks_dev;
create database tasks_test;
CREATE ROLE admin WITH LOGIN ENCRYPTED PASSWORD '31415';
\q
`

##### TEST AND COVERAGE

TEST (предварительно создайте БД - `create database tasks_test;`)
`bash
yarn run test
`

COVERAGE (предварительно создайте БД - `create database tasks_test;`)
`bash
yarn run coverage
`

##### LINT

`bash
yarn run lint
`

##### MIGRATIONS AND SEEDS

TEST (предварительно создайте БД - `create database tasks_test;`)
`bash
NODE_ENV=test sequelize db:migrate:undo:all // Отменить все миграции
NODE_ENV=test sequelize db:migrate // Применить все миграции
NODE_ENV=test sequelize db:seed:all // Заполнить БД даными
`

DEVELOPMENT (предварительно создайте БД - `create database tasks_dev;`)
`bash
NODE_ENV=development sequelize db:migrate:undo:all // Отменить все миграции
NODE_ENV=development sequelize db:migrate // Применить все миграции
NODE_ENV=development sequelize db:seed:all // Заполнить БД даными
`

PRODACTION (предварительно создайте БД - `create database tasks;`)
`bash
NODE_ENV=production sequelize db:migrate:undo:all // Отменить все миграции
NODE_ENV=production sequelize db:migrate // Применить все миграции
NODE_ENV=production sequelize db:seed:all // Заполнить БД даными
`

##### INIT DB BY DEFAULT

##### DOCUMENTS

[REST API документация](https://kozhevnikov-examples.gitlab.io/nodejs-backend/)

MANUAL
`bash
yarn run doc
`
